import pg from 'pg';
import config from './config.js';
import express from 'express';
import userAuth from './routes/userAuth';
import animal from './routes/animal';
import photo from './routes/photo';
import shelter from './routes/shelter';
import notifications from './routes/notifications';
import message from './routes/message';
import documents from './routes/documents';

// BASE SETUP
// =============================================================================

// call the packages we need
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

var busboy = require('connect-busboy');
app.use(busboy()); 

const fs = require('fs');

var port = process.env.PORT || 5599;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);

//postgres
export const pool = new pg.Pool(config.postgres);

var AWS = require('aws-sdk');
AWS.config.loadFromPath('./app/configAWS.json');
export const s3 = new AWS.S3();

// router.route('/users/:userid')
//   .get(userAuth.getUser);

router.route('/users')
  .post(userAuth.postUser);

router.route('/users/check')
  .post(userAuth.checkUser);

router.route('/sessions/create')
  .post(userAuth.createSession);

router.route('/test')
  .get(userAuth.test);

router.route('/animals/add')
  .post(animal.postAnimal);

router.route('/animals/:shelterId')
  .get(animal.getAnimals);

router.route('/animal/:animalId')
  .get(animal.getAnimal);

router.route('/animals/edit/:animalId')
  .post(animal.editAnimal);
  
router.route('/animals/adoption/:province/:species')
  .get(animal.getAnimalsBySpeciesProvince);

router.route('/adoption/animal/:animalId')
  .get(animal.getAdoptionAnimal);

router.route('/vaccination/:animalId')
  .get(animal.getVaccination);

router.route('/vaccination/add')
  .post(animal.postVaccination);

router.route('/vaccination/edit/:vaccId')
  .post(animal.editVaccination);

router.route('/treatment/:animalId')
  .get(animal.getTreatment);

router.route('/treatment/add')
  .post(animal.postTreatment);

router.route('/treatment/edit/:treatmentId')
  .post(animal.editTreatment);

router.route('/photos/:animalId')
  .get(photo.getAnimalPhoto);

router.route('/photos/add/:animalId')
  .post(photo.postAnimalPhoto);

router.route('/shelter/:shelterId')
  .get(shelter.getShelter);

router.route('/shelter/edit/:shelterId')
  .post(shelter.editShelter);

router.route('/provinces')
  .get(shelter.getProvinceList);

router.route('/notifications/info/:shelterId')
  .get(notifications.getMissingInfo);

router.route('/notifications/vacc/:shelterId')
  .get(notifications.getComingVaccinations);

router.route('/notifications/treat/:shelterId')
  .get(notifications.getComingTreatments);

router.route('/message/new')
  .post(message.postMessage);

router.route('/messages/unanswered/:shelterId')
  .get(message.getUnansweredMessages);

router.route('/messages/all/:shelterId')
  .get(message.getAllMessages);
  
router.route('/message/edit/:messageId')
  .post(message.editMessage);

router.route('/messages/user')
  .post(message.getUserMessages);

router.route('/docs/adoption')
  .post(documents.getAdoptionDocument);

