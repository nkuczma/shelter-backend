import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { pool } from '../server.js';

const jwtSecret = 'tajnehaslo';

export function createToken(data) {
  return jwt.sign(data, jwtSecret, { expiresIn: 24*60*60*10000 });
}

export function decodeToken(token) {
  return jwt.decode(token, jwtSecret);
}

//testowa funkcja
// const getUser = (req, res) => {
//   var id = req.params.userid;
//   pool.connect(function(err, client, done) {
//     if(err) {
//       return console.error('error fetching client from pool', err);
//     }

//     pool.query('SELECT user_id, user_name, password FROM shelter."user" where user_id =$1', [id], function(err, result) {
//       done();
//       if(err) {
//         return console.error('error running query', err);
//       }
//       res.json(result.rows[0]);
//     });
//   });
// };

const postUser = (req, res) => {
  var data = req.body;
  var admin_data = decodeToken(data.token);
  if(!admin_data || admin_data.role!="admin") {
    return res.status(400).send("You have no right to post user");
  }
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
  pool.query('SELECT user_id FROM shelter."user" WHERE user_name = ($1);', [data.user_name], function(err, result) {
    if (err) return console.log(err);
      if(result.rows[0]) {
        return res.status(400).send("duplicate username");
      }
  });
  if (!data.user_name || !data.password) {
    return res.status(400).send("You must send the username and the password");
  }
  else {
        bcrypt.hash(data.password, 8, (err,hash) => {
          pool.query('INSERT INTO shelter."user" VALUES (DEFAULT, $1, $2, $3, $4, $5, $6) RETURNING user_id',
          [admin_data.shelter_id, data.user_name, hash, "user", data.first_name, data.last_name], function(err, result) {
            done();
            if(err) {
              return console.error('error running query', err);
            }
            let user = {
              user_name: data.user_name
            }

            res.status(201).send("Zarejestrowano");
          });
        });
      }
  });
};

const createSession = (req, res) => {
  const data = req.body;
  if (!data.user_name || !data.password) {
    return res.status(400).send("Musisz podać nazwę użytkownika i hasło");
  }
  pool.query('SELECT user_id, password, shelter_id, role FROM shelter."user" WHERE user_name = ($1);', [data.user_name], function(err, result) {
    if (err) return console.log(err);
      if(result.rows[0]) {
        var pass = result.rows[0].password;
        bcrypt.compare(data.password, pass, function(err, isMatch) {
          if(isMatch){
            console.log(result.rows[0].role);
            return res.status(201).send({
              id_token: createToken(result.rows[0]),
              user_name: data.user_name,
              shelter_id: result.rows[0].shelter_id,
              role: result.rows[0].role
            });
          }
          else {
            return res.status(401).send("Złe hasło");
          }
        });
      }
      else {
        return res.status(401).send("Brak takiego użytkownika");
      }
  });
};

const checkUser = (req, res) => {
  const token = req.body.token;
  const data = token? decodeToken(token) : null;
  if (!data || !data.user_id || !data.password) {
    return res.status(400).send("Musisz podać nazwę użytkownika i hasło");
  }
  pool.query('SELECT user_name, password, role, shelter_id FROM shelter."user" WHERE user_id = ($1);', [data.user_id], function(err, result) {
    if (err) return console.log(err);
      if(result.rows[0]) {
        var pass = result.rows[0].password;
        if(data.password === pass){
          return res.status(201).send({
            exists: true,
            user_name: result.rows[0].user_name,
            shelter_id: result.rows[0].shelter_id,
            role: result.rows[0].role
          });
        }
        else {
          return res.status(201).send({
            exists: false
          });
        }
      }
      else {
        return res.status(401).send("Brak takiego użytkownika");
      }
  });
};

const test = (req, res) => { return "test"; }

export default {
  postUser,
  // getUser,
  createSession,
  checkUser,
  test
};