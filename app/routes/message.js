import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
import { pool } from '../server.js';

const jwtSecret = 'tajnehaslo';

export function decodeToken(token) {
  return jwt.decode(token, jwtSecret);
}

function mapObj(object,fn) {
  return Object.keys(object)
  .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key], object)}),
          {});
}

const postMessage = (req, res) => {
  var data = req.body;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('INSERT INTO shelter."message" VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7)',
        [
          data.shelter_id,
          data.animal_id,
          data.user_name,
          data.user_mail,
          data.user_phone,
          data.message_content,
          data.date_send,
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.json('ok');
        });
      }
  });
};

const getUnansweredMessages = (req, res) => {
  var id = req.params.shelterId;
  var token = req.headers.authorization? req.headers.authorization : null;
  var user_data = decodeToken(token);
  if(user_data && user_data.shelter_id === id) {
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }
      pool.query('SELECT * FROM shelter."message" WHERE shelter_id=$1 AND user_id IS NULL ORDER BY message_id', [id], function(err,result) {
        done();
        if(err) {
          return console.error('no data', err);
        }
        if(result.rows.length<1) { res.status(400).send("no data"); }
        res.json(result.rows);
      });
    });
  }
};

const editMessage = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);
  var userId;
  if( user_data ) {
    userId = parseInt(user_data.user_id);
  }
  var message_id = req.params.messageId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('UPDATE shelter."message" SET user_id=COALESCE($2,user_id), comment=COALESCE($3,comment), date_end=COALESCE($4,date_end)  WHERE message_id=$1',
        [
          message_id,
          userId,
          data.comment,
          data.date_end
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.json('ok');
        });
      }
  });
};

const getUserMessages = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);
  if(!user_data) {
    res.status(400).send("no rights");
  }
  else {
    var userId = user_data.user_id
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }
      pool.query('SELECT * FROM shelter."message" WHERE user_id=$1', [userId], function(err,result) {
        done();
        if(err) {
          return console.error('no data', err);
        }
        if(result.rows.length<1) { res.status(400).send("no data"); }
        res.json(result.rows);
      });
    });
  }
};

const getAllMessages = (req, res) => {
  var id = req.params.shelterId;
  var token = req.headers.authorization? req.headers.authorization : null;
  var user_data = decodeToken(token);
  if(user_data && user_data.shelter_id === id && user_data.role==="admin") {
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }
      pool.query('SELECT * FROM shelter."message" WHERE shelter_id=$1 ORDER BY message_id', [id], function(err,result) {
        done();
        if(err) {
          return console.error('no data', err);
        }
        if(result.rows.length<1) { res.status(400).send("no data"); }
        res.json(result.rows);
      });
    });
  }
};
export default {
  postMessage,
  getUnansweredMessages,
  editMessage,
  getUserMessages,
  getAllMessages
};