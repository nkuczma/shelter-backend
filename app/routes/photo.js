import { pool } from '../server.js';
import { s3 } from '../server.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const fs = require('fs');
const jwtSecret = 'tajnehaslo';

export function decodeToken(token) {
  return jwt.decode(token, jwtSecret);
}

function mapObj(object,fn) {
  return Object.keys(object)
  .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key], object)}),
          {});
}

const getAnimalPhoto = (req, res) => {
  var id = req.params.animalId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT image_url FROM shelter."animal" WHERE animal_id=$1', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      else if(result.rows.length<1) { res.status(400).send("no data"); }
      else {
        let imgUrl = result.rows[0].image_url;
        var urlParams = {Bucket: 'sheltermanagementcontainer', Key: imgUrl};
        s3.getSignedUrl('getObject',urlParams, function(err, url){
          res.json(url);
        });
      }
    });
  });
};

const postAnimalPhoto = (req, res) => {
  var token = req.headers.authorization? req.headers.authorization : null;
  if(token) {
    pool.connect(function(err, client, done) {
      var user_data = decodeToken(token);
      if(!user_data.user_id) {
        return res.status(400).send("You have no right to post photo");
      }
      var fstream;
      req.pipe(req.busboy);
      req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        var id = req.params.animalId;
        var ext = filename.match(/\.([A-Za-z]+)$/i)[1];
        var imgUrl = id + '.' + ext;
          var data = {
            Key: imgUrl, 
            Body: file, 
            Bucket: 'sheltermanagementcontainer',
            ContentType: mimetype
          };
          s3.upload(data, function(err, data){
            if (err) 
              { return res.status(400).send("err");
              } else {
                
                pool.query('UPDATE shelter."animal" SET image_url=COALESCE($1,image_url) WHERE animal_id=$2',
                [
                  imgUrl,
                  id,
                ], 
                function(err, result) {
                  done();
                  if(err) {
                    return console.error('error running query', err);
                  }
                  else {
                    res.status(201).send("Zarejestrowano");
                  }
                });
              }
          });
        });
    });
  }
  else {
    return res.status(400).send("You have no right to post photo");
  }
};

export default {
  getAnimalPhoto,
  postAnimalPhoto
};