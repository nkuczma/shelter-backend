import { pool } from '../server.js';
import { s3 } from '../server.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const jwtSecret = 'tajnehaslo';

export function decodeToken(token) {
  return jwt.decode(token, jwtSecret);
}

function mapObj(object,fn) {
  return Object.keys(object)
  .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key], object)}),
          {});
}

function filterObj(object,fn) {
  return Object.keys(object)
    .filter( key => {
      const result = fn(key, object[key], object);
      return result === false || result === null;
    })
    .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key],object)}),
            {});
}

const getMissingInfo = (req, res) => {
  var id = req.params.shelterId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."animal" WHERE shelter_id=$1 AND ( ( registry_number IS NULL AND tatoo_id IS NULL AND microchip IS NULL ) OR state IS NULL OR age IS NULL OR gender IS NULL OR size IS NULL OR name IS NULL OR description IS NULL OR image_url IS NULL)', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      let val = result.rows;
      let missingInfo = [];
      val = val.map((val) => { 
        var animal = {};
        animal.id = val.animal_id;
        animal.name = val.name;
        animal.missing = [];
        filterObj(val, (key,val,obj) => {
          if ( val===null && key!='leave_date' && key!='tatoo_id' && key!='felv_test' && key!='enter_date' && key!='quarantine' && key!='for_adoption' ){
            animal.missing.push(key); 
          }
        });
        missingInfo.push(animal);
      });
      res.json(missingInfo);
    });
  });
};


const getComingVaccinations = (req, res) => {
  var id = req.params.shelterId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT shelter."vaccination".vacc_name, shelter."vaccination".next_vacc, shelter."animal".name, shelter."animal".animal_id FROM shelter."vaccination" JOIN shelter."animal" ON shelter."vaccination".animal_id=shelter."animal".animal_id WHERE shelter_id=$1 AND next_vacc>now();', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      res.json(result.rows);
    });
  });
};

const getComingTreatments = (req, res) => {
  var id = req.params.shelterId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT shelter."treatment".treat_name, shelter."treatment".next_med, shelter."animal".name, shelter."animal".animal_id FROM shelter."treatment" JOIN shelter."animal" ON shelter."treatment".animal_id=shelter."animal".animal_id WHERE shelter_id=$1 AND next_med>now();', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      res.json(result.rows);
    });
  });
};


export default {
  getMissingInfo,
  getComingVaccinations,
  getComingTreatments
};