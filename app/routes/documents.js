import { pool } from '../server.js';

function mapObj(object,fn) {
  return Object.keys(object)
  .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key], object)}),
          {});
}

var async = require ( 'async' );
var officegen = require('officegen');
//?
var fs = require('fs');
var path = require('path');

const getAdoptionDocument = (req, res) => {
  var data = req.body;
  if(data) {
    var docx = officegen ( {
      type: 'docx',
      orientation: 'portrait',
      pageMargins: { top: 1000, left: 1000, bottom: 1000, right: 1000 }
    } );
    docx.on ( 'error', function ( err ) {
      console.log ( err );
    });

    var pObj = docx.createP ( { align: 'center' } );

    pObj.addText ( 'UMOWA ADOPCYJNA' , {font_size: 18, bold: true});
    pObj = docx.createP ();
    pObj.addText(' ');
    pObj = docx.createP();
    var date = new Date();
    console.log(date.toLocaleDateString());
    pObj.addText(
      'Zawarta dnia ' + 
      date.toLocaleDateString('pl') + 
      ' w: ' + 
      data.city + 
      ', pomiędzy: ');
      pObj.addText ( data.name_full + ", " + data.address, { bold: true });
      pObj.addText ( ' zwanego dalej Schroniskiem, a ' + 
      'Panią/Panem ..................................., zam. w................................... przy ul............................................... legitymującą/ym się dowodem osobistym nr ............................................, nr tel.: ............................................ zwana dalej Opiekunem, o następującej treści:'
    );

    pObj = docx.createP ( { align: 'center', bold: true } );
    pObj.addText ( '§1', { bold: true } );
    pObj = docx.createP();
    pObj.addText('1. Schronisko jest pełnoprawnym właścicielem zwierzęcia, stanowiącego przedmiot niniejszej umowy.');
    pObj.addLineBreak ();
    pObj.addText('2. Schronisko przekazuje Opiekunowi ');
      var species;
      if(data.species==="dog") species = "psa";
      else if(data.species==="cat") species = "kota"
      else species = "zwierzę"
    pObj.addText(species + ' o danych:');
    pObj.addLineBreak ();
    pObj.addText('Nr rejestru: ');
    pObj.addText(data.registry_number? data.registry_number : "-", { bold: true });
    pObj.addText(', numer tatuażu: ');
    pObj.addText(data.tatoo_id? data.tatoo_id : "-", { bold: true });
    pObj.addText(', microchip: ');
    pObj.addText(data.microchip? data.microchip : "-", { bold: true });
    pObj.addText(', zwanym w dalszej części umowy zwierzęciem.');

    pObj = docx.createP ({ align: 'center', bold: true });
    pObj.addText('§2', { bold: true });
    pObj = docx.createP();
    pObj.addText('Opiekun określa miejsce stałego pobytu zwierzęcia pod adresem:  ............................................................................................................ i zobowiązuje się do pisemnego powiadomienia Schroniska w terminie do 7 dni o zmianie adresu pobytu zwierzęcia, (nie dotyczy okazjonalnych i krótkich wyjazdów).');
    
    pObj = docx.createP ({ align: 'center', bold: true });
    pObj.addText('§3', { bold: true });
    pObj = docx.createP();
    pObj.addText('Opiekun zobowiązuje się otoczyć właściwą opieką adoptowanego psa, w tym w szczególności:');
    pObj.addLineBreak ();
    pObj.addText(' 1. nie pozbędzie się, nie sprzeda, nie porzuci, nie odda zwierzęcia osobom trzecim, ');
    pObj.addLineBreak ();
    pObj.addText(' 2. zapewni zwierzęciu wikt, warunki do zdrowego życia i będzie go traktował z należnym istocie żywej szacunkiem, ');
    pObj.addLineBreak ();
    pObj.addText(' 3. nie będzie trzymał zwierzęcia na uwięzi (łańcuchu, lince, bądź innym postronku) i zapewni zwierzęciu możliwość swobodnego biegania na terenie posesji oraz codzienne spacery, ');
    pObj.addLineBreak ();
    pObj.addText(' 4. zagwarantuje zwierzęciu niezbędną opiekę medyczną (w tym coroczne szczepienia zwierzęcia i odrobaczanie wraz z poddaniem zwierzęcia badaniu ogólnego stanu zdrowia u lek. weterynarii z wpisem do książeczki), ');
    pObj.addLineBreak ();
    pObj.addText(' 5. zapewni zwierzęciu bezpieczeństwo, włączając w to właściwe zabezpieczenie miejsca i terenu, na którym przebywa, zapobiegając jego ucieczce, przypadkowym wyjściom, pogryzieniem, kradzieżą, ');
    pObj.addLineBreak ();
    pObj.addText(' 6. zaopatrzy zwierzę w obrożę z adresem i numerem telefonu, ');
    pObj.addLineBreak ();
    pObj.addText(' 7. podda zwierzę zabiegowi kastracji/sterylizacji, w terminie 3 miesięcy od dnia podpisania niniejszej umowy, a szczeniaka/kocię po ukończeniu przez niego 1 roku lecz nie później niż do ukończenia przez niego 1,5 roku oraz przedstawi w terminie 7 dni zaświadczenie wydane przez lekarza weterynarii o przeprowadzonym zabiegu osobiście, bądź prześle je na adres schroniska, ');
    pObj.addLineBreak ();
    pObj.addText(' 8. nie będzie doprowadzał do rozmnażania psa i zapobiegnie niekontrolowanemu rozrodowi, jeśli zaistnieją przeciwwskazania zdrowotne do przeprowadzenia zabiegu sterylizacji/kastracji. ');
    
    pObj = docx.createP ({ align: 'center', bold: true });
    pObj.addText('§4', { bold: true });
    pObj = docx.createP();
    pObj.addText('1. Schronisko ma prawo przeprowadzenia zapowiedzianych i niezapowiedzianych kontroli w miejscu pobytu zwierzęcia, podczas których Opiekun musi okazać zwierzę i książeczkę zdrowia do wglądu. ');
    pObj.addLineBreak ();
    pObj.addText('2. Opiekun zobowiązuje się umożliwić przeprowadzenie kontroli osobom reprezentującym Schronisko.');
    pObj.addLineBreak ();
    pObj.addText('3. W razie stwierdzenia nieodpowiednich warunków, Schronisko ma prawo odebrać zwierzę Opiekunowi. Opiekun zobowiązuje się w takim przypadku bezzwłocznie wydać zwierzę wraz z dokumentami otrzymanymi przy adopcji. Stwierdzenie i ocena warunków utrzymywania zwierzęcia przysługuje wyłącznie Schronisku.');
    pObj.addLineBreak ();
    pObj.addText('4. Opiekun przyjmuje do wiadomości, że w razie złego traktowania zwierzęcia grozi mu odpowiedzialność karna na podstawie Ustawy z dnia 21.08.1997 r. o ochronie zwierząt.');

    pObj = docx.createP ({ align: 'center', bold: true });
    pObj.addText('§5', { bold: true });
    pObj = docx.createP();
    pObj.addText('1. Zwierzę staje się własnością Opiekuna po podpisaniu niniejszej umowy.');
    pObj.addLineBreak ();
    pObj.addText('2. Całkowita odpowiedzialność oraz koszty utrzymania i opieki nad zwierzęciem spoczywają na Opiekunie od dnia podpisania niniejszej umowy.');
    pObj.addLineBreak ();
    pObj.addText('3. Opiekun oświadcza, że stan zwierzę jest mu znane, zapoznał się z nim i nie rości w związku z nim żadnych pretensji.');

    pObj = docx.createP ({ align: 'center', bold: true });
    pObj.addText('§6', { bold: true });
    pObj = docx.createP();
    pObj.addText('W przypadku zmiany warunków życiowych i braku możliwości dalszej opieki nad zwierzęciem Opiekun niezwłocznie powiadomi Schronisko.');

    pObj = docx.createP ({ align: 'center', bold: true });
    pObj.addText('§7', { bold: true });
    pObj = docx.createP();
    pObj.addText('W przypadku niedotrzymania któregokolwiek z warunków Umowy Adopcyjnej, zwierzę przechodzi z powrotem na własność Schroniska bez dodatkowych oświadczeń woli, a Opiekun zobowiązuje się do bezzwłocznego wydania zwierzę osobie reprezentującej Schronisko.');

    pObj = docx.createP ({ align: 'center', bold: true });
    pObj.addText('§8', { bold: true });
    pObj = docx.createP();
    pObj.addText('Umowę sporządzono w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.');

    pObj = docx.createP ();
    pObj.addText(' ');
    pObj = docx.createP ();
    pObj.addText('.............................................                                                                   .............................................');
    pObj = docx.createP ();
    pObj.addText('     (czytelny podpis Opiekuna)                                                                          (podpis osoby reprezentującej Schronisko)', {font_size: '9'});

    docx.on('finalize', function(written) {
        console.log('Finish to create Word file. Total bytes created: ' + written);
    });
    docx.on('error', function(err) {
        console.log(err);
    });

    res.writeHead ( 200, {
      "Content-Type": "application/vnd.openxmlformats-officedocument.documentml.document",
      'Content-disposition': 'attachment; filename=umowa.docx'
    });

    docx.generate(res);
  }
};

export default {
  getAdoptionDocument
};