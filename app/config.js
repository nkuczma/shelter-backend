const config = {
  port: process.env.PORT || 7559,
  postgres: {
    user: 'shelter_user',
    database: 'shelter',
    password: 'admin',
    host: 'localhost',
    port: 5432,
    max: 20,
    idleTimoutMillis: 30000
  }
};

export default config;
