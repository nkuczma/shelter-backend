import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { pool } from '../server.js';

const jwtSecret = 'tajnehaslo';

export function decodeToken(token) {
  return jwt.decode(token, jwtSecret);
}

function mapObj(object,fn) {
  return Object.keys(object)
  .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key], object)}),
          {});
}

const getShelter = (req, res) => {
  var id = req.params.shelterId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."shelter" JOIN shelter."province" ON shelter."province".province_id=shelter."shelter".province  WHERE shelter_id=$1', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      if(result.rows.length<1) { res.status(400).send("no data"); }
      res.json(result.rows[0]);
    });
  });
};

const editShelter = (req, res) => {
  var token = req.headers.authorization? req.headers.authorization : null;
  var user_data = decodeToken(token);
  var data = req.body;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    else if(!user_data) {
      return res.status(400).send("You have no right to edit shelter");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('UPDATE shelter."shelter" SET name_full=COALESCE($1,name_full), name_short=COALESCE($2, name_short), city=COALESCE($3, city), province=COALESCE($4, province), address=COALESCE($5, address), phone=COALESCE($6, phone), zipcode=COALESCE($7, zipcode) WHERE shelter_id=$8',
        [
            data.name_full,
            data.name_short,
            data.city,
            data.province,
            data.address,
            data.phone,
            data.zipcode,
            req.params.shelterId
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.status(201).send("Zarejestrowano");
        });
      }
  });
};

const getProvinceList = (req, res) => {
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."province"', function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      res.json(result.rows);
    });
  });
};

export default {
  getShelter,
  editShelter,
  getProvinceList
};