# Shelter Management backend

Backend application made for ShelterManagement that provides REST API. API is divided into seven parts: 

* Animal - all information about animals in shelters
* Document - part responsible for generating filled document (docx) with animal informations for adoption agreement
* Message - messages information
* Notifications - reminders for vaccinations and treatments
* Photo - storing ang getting animals' photos
* Shelter - information about shelters getting part in the system
* UserAuth - login actions, creating new users

## Used technologies

App was made with Node.js and Express.js. JWT tokens are made for login sessions, all paswords are encrypted and decrypted with bcrypt algorithm. Amazon Web Services S3 is used for storing and getting photos.

## Launching

To install bcrypt package, it is required to have installed Python and for Windows users - C++ and C# tools.

Launching is by the command `npm start`. App is on `http://localhost:4200/`.