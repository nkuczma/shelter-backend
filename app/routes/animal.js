import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
import { pool } from '../server.js';

const jwtSecret = 'tajnehaslo';

export function decodeToken(token) {
  return jwt.decode(token, jwtSecret);
}

// function filterObj(object,fn) {
//   return Object.keys(object)
//     .filter( key => {
//       const result = fn(key, object[key], object);
//       return result === false || result === null;
//     })
//     .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key],object)}),
//             {});
// }
 
function mapObj(object,fn) {
  return Object.keys(object)
  .reduce((obj, key) => Object.assign(obj, {[key]: fn(key, object[key], object)}),
          {});
}

const postAnimal = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);

  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else if(!user_data) {
      return res.status(400).send("You have no right to post animal");
    }
    else {
     // data = _.mapValues(data, function(val) { return val? val : null; });
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('INSERT INTO shelter."animal" VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16) RETURNING animal_id',
        [
          user_data.shelter_id, 
          data.registry_number, 
          data.tatoo_id, 
          data.microchip, 
          data.species,
          data.state,
          data.for_adoption,
          data.felv_test,
          data.age,
          data.gender,
          data.size,
          data.enter_date,
          data.leave_date,
          data.quarantine,
          data.name,
          data.description
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.json(result.rows[0]);
        });
      }
  });
};

const getAnimals = (req, res) => {
  var id = req.params.shelterId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."animal" WHERE shelter_id=$1 ORDER BY animal_id', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      if(result.rows.length<1) { res.status(400).send("no data"); }
      res.json(result.rows);
    });
  });
};

const editAnimal = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);

  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else if(!user_data) {
      return res.status(400).send("You have no right to post animal");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('UPDATE shelter."animal" SET registry_number=COALESCE($1,registry_number), tatoo_id=COALESCE($2,tatoo_id), microchip=COALESCE($3,microchip), species=COALESCE($4,species), state=COALESCE($5,state), for_adoption=COALESCE($6,for_adoption), felv_test=COALESCE($7,felv_test), age=COALESCE($8,age), gender=COALESCE($9,gender), size=COALESCE($10,size), enter_date=COALESCE($11,enter_date), leave_date=COALESCE($12,leave_date), quarantine=COALESCE($13,quarantine), name=COALESCE($14,name), description=COALESCE($15,description) WHERE animal_id=$16',
        [
          data.registry_number, 
          data.tatoo_id, 
          data.microchip, 
          data.species,
          data.state,
          data.for_adoption,
          data.felv_test,
          data.age,
          data.gender,
          data.size,
          data.enter_date,
          data.leave_date,
          data.quarantine,
          data.name,
          data.description,
          req.params.animalId
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.status(201).send("Zarejestrowano");
        });
      }
  });
};

const getAnimal = (req, res) => {
  var id = req.params.animalId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."animal" WHERE animal_id=$1 ', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      if(result.rows.length<1) { res.status(400).send("no data"); }
      res.json(result.rows);
    });
  });
};

const getVaccination = (req, res) => {
  var id = req.params.animalId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."vaccination" WHERE animal_id=$1 ORDER BY vacc_id', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      if(result.rows.length<1) { res.status(400).send("no data"); }
      res.json(result.rows);
    });
  });
};

const postVaccination = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);

  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else if(!user_data) {
      return res.status(400).send("You have no right to post animal");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('INSERT INTO shelter."vaccination" VALUES (DEFAULT, $1, $2, $3, $4)',
        [
          data.animal_id,
          data.vacc_name,
          data.vacc_date,
          data.next_vacc
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.status(201).send("Zarejestrowano");
        });
      }
  });
};

const editVaccination = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);

  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else if(!user_data) {
      return res.status(400).send("You have no right to post animal");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('UPDATE shelter."vaccination" SET vacc_name=COALESCE($1,vacc_name), vacc_date=COALESCE($2, vacc_date), next_vacc=COALESCE($3, next_vacc) WHERE vacc_id=$4',
        [
          data.vacc_name,
          data.vacc_date,
          data.next_vacc,
          req.params.vaccId
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.status(201).send("Zarejestrowano");
        });
      }
  });
};

const getTreatment = (req, res) => {
  var id = req.params.animalId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."treatment" WHERE animal_id=$1 ORDER BY treatment_id', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      if(result.rows.length<1) { res.status(400).send("no data"); }
      res.json(result.rows);
    });
  });
};

const postTreatment = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);

  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else if(!user_data) {
      return res.status(400).send("You have no right to post animal");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('INSERT INTO shelter."treatment" VALUES (DEFAULT, $1, $2, $3, $4, $5, $6)',
        [
          data.animal_id,
          data.treatment_date,
          data.medicine,
          data.is_treated,
          data.next_med,
          data.treat_name
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.status(201).send("Zarejestrowano");
        });
      }
  });
};

const editTreatment = (req, res) => {
  var data = req.body;
  var user_data = decodeToken(data.token);

  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if (!data) {
      return res.status(400).send("no data");
    }
    else if(!user_data) {
      return res.status(400).send("You have no right to post animal");
    }
    else {
     data = mapObj(data,(key, val, obj) => { return val? val : null; });
      pool.query('UPDATE shelter."treatment" SET treatment_date=COALESCE($1,treatment_date), medicine=COALESCE($2,medicine), is_treated=COALESCE($3,is_treated), next_med=COALESCE($4,next_med), treat_name=COALESCE($5, treat_name) where treatment_id=$6',
        [
          data.treatment_date,
          data.medicine,
          data.is_treated,
          data.next_med,
          data.treat_name,
          req.params.treatmentId
        ], 
        function(err, result) {
          done();
          if(err) {
            return console.error('error running query', err);
          }
          res.status(201).send("Zarejestrowano");
        });
      }
  });
};

// do refaktoryzacji kiedyś
const getAnimalsBySpeciesProvince = (req, res) => {
  var species = req.params.species;
  var province = req.params.province;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    if(species==="all") {
      if(province==="all") {
        pool.query('SELECT * FROM shelter."animal" WHERE state=$1;', ['for_adopt'], function(err,result) {
          done();
          if(err) {
            return console.error('no data', err);
          }
          if(result.rows.length<1) { res.status(400).send("no data"); }
          res.json(result.rows);
        });
      }
      else {
        pool.query('SELECT * FROM shelter."animal" JOIN shelter."shelter" ON shelter."animal".shelter_id=shelter."shelter".shelter_id WHERE province=$1 AND state=$2', [province, 'for_adopt'], function(err,result) {
          done();
          if(err) {
            return console.error('no data', err);
          }
          if(result.rows.length<1) { res.status(400).send("no data"); }
          res.json(result.rows);
        });
      }
    }
    else {
      if(province==="all") {
        pool.query('SELECT * FROM shelter."animal" WHERE species=$1 AND state=$2',[species, 'for_adopt'],function(err,result) {
          done();
          if(err) {
            return console.error('no data', err);
          }
          if(result.rows.length<1) { res.status(400).send("no data"); }
          res.json(result.rows);
        });
      }
      else {
        pool.query('SELECT * FROM shelter."animal" JOIN shelter."shelter" ON shelter."animal".shelter_id=shelter."shelter".shelter_id WHERE species=$1 AND province=$2 AND state=$3',[species, province, 'for_adopt'],function(err,result) {
          done();
          if(err) {
            return console.error('no data', err);
          }
          if(result.rows.length<1) { res.status(400).send("no data"); }
          res.json(result.rows);
        });
      }
    }
  });
};

const getAdoptionAnimal = (req, res) => {
  var id = req.params.animalId;
  pool.connect(function(err, client, done) {
    if(err) {
      return console.error('error fetching client from pool', err);
    }
    pool.query('SELECT * FROM shelter."animal" JOIN shelter."shelter" ON shelter."animal".shelter_id=shelter."shelter".shelter_id  WHERE animal_id=$1 ', [id], function(err,result) {
      done();
      if(err) {
        return console.error('no data', err);
      }
      if(result.rows.length<1) { res.status(400).send("no data"); }
      res.json(result.rows);
    });
  });
};

export default {
  postAnimal,
  getAnimals,
  getAnimal,
  getAdoptionAnimal,
  getVaccination,
  postVaccination,
  getTreatment,
  postTreatment,
  editAnimal,
  editVaccination,
  editTreatment,
  getAnimalsBySpeciesProvince
};